<?php
use Vein\Http\Json;
use Vein\Http\Status;
use Vein\Http\Utility;

/**
 * Class OaStripe_FrontController
 */
class OaStripe_FrontController extends \Website\Controller\Action
{

    protected $httpJson;
    protected $httpUtility;

    /**
     * @return mixed
     */
    public function getHttpParams($key = null)
    {
        $data = $this->getHttpUtility()->getJsonData();
        if (null === $key) {
            return $data;
        }

        return $data[$key];
    }

    /**
     * @return Utility
     */
    public function getHttpUtility()
    {
        if (null === $this->httpUtility) {
            $this->httpUtility = new Utility($this->getRequest());
        }

        return $this->httpUtility;
    }

    /**
     * @param \Zend_Form $form
     * @return mixed
     */
    public function getValidValues(\Zend_Form $form, $params = null)
    {
        if (!$this->getRequest()->isPost()) {
            $this->getResponder()->error(Status::HTTP_BAD_REQUEST)->flush();
        }

        //Get the post values
        if (null === $params) {
            $params = $this->getHttpUtility()->getJsonData();
        }

        if (!$form->isValid($params)) {
            $messages = $form->getMessages();
            $this->getResponder()->error(Status::HTTP_UNPROCESSABLE_ENTITY)->setData($messages)->debug($messages)->flush();
        }

        return $form->getValues();
    }

    /**
     * @return Json
     */
    public function getResponder()
    {
        if (null === $this->httpJson) {
            $this->httpJson = new Json();
        }

        return $this->httpJson;
    }

    /**
     * Pre Dispatch
     */
    public function preDispatch()
    {
        parent::preDispatch();
    }

    /**
     * Default Action
     */
    public function defaultAction()
    {
    }

    /**
     * Confirm Action
     */
    public function confirmAction()
    {
        //echo \OaStripe\Stripe::newTransaction('tester5@oasandbox.info', 'invest', 5);
        // tx_57ff4b2e54b5d

        $tx = strip_tags($this->getParam('tx'));
        if (empty($tx)) {
            throw new \Exception('Invalid transaction id');
        }

        $transaction = \OaStripe\Stripe::getTransaction($tx);
        if (empty($transaction)) {
            throw new \Exception('Transaction expired');
        }

        //verify plan
        $dataPersister = new \OaStripe\Db\DataPersister();
        $plan = $dataPersister->findPlanById($transaction['plan']);

        if (!$plan) {
            throw new \Exception('Invalid plan');
        }
        $planArray = $plan->toArray();

        $this->view->plan = $planArray;
        $this->view->quantity = $transaction['quantity'];
        $this->view->email = $transaction['email'];
        $this->view->do = $transaction['do'];
        $this->view->tx = $tx;

        $this->view->total = ($planArray['amount'] / 100) * $transaction['quantity'];
    }

    /**
     * Token Action
     */
    public function tokenAction()
    {
        $response = $this->getResponder();

        try {
            $params = $this->getRequest()->getPost();
            $form = $params['form'];

            $tx = strip_tags($form['tx']);
            if (empty($tx)) {
                throw new \Exception('Invalid transaction id');
            }

            $transaction = \OaStripe\Stripe::getTransaction($tx);
            if (empty($transaction)) {
                throw new \Exception('Transaction expired');
            }

            if ($transaction['complete']) {
                throw new \Exception('Transaction complete or expired');
            }

            //verify plan
            $dataPersister = new \OaStripe\Db\DataPersister();
            $plan = $dataPersister->findPlanById($transaction['plan']);

            if (!$plan) {
                throw new \Exception('Invalid plan');
            }

            if (empty($params['id'])) {
                throw new \Exception('Invalid token id');
            }

            //Charge the customer
            $planArray = $plan->toArray();

            try {

                $customer = \Stripe\Customer::create([
                    "source" => $params['id'],
                    "email" => $transaction['email']
                ]);
                $dataPersister->createCustomer($customer);
                $customerArray = $customer->jsonSerialize();

                $subscription = \Stripe\Subscription::create([
                    "customer" => $customerArray['id'],
                    "plan" => $planArray['id'],
                    'quantity' => $transaction['quantity']
                ]);
                $dataPersister->createSubscription($subscription);

                $transaction['complete'] = true;
                \OaStripe\Stripe::updateTransaction($tx, $transaction);

                $subscriptionArray = $subscription->jsonSerialize();

                $data = [
                    'done' => true
                ];

                if ($redirectUrl = \OaStripe\Stripe::getRedirectUrl()) {
                    $data['redirect'] = $redirectUrl . '?' . http_build_query($form);
                }
                $response->setData($data)->success();

            } catch (\Stripe\Error\Card $e) {
                // Since it's a decline, \Stripe\Error\Card will be caught

                \Pimcore\Logger::critical($e);
                $body = $e->getJsonBody();
                $err = $body['error'];

                //print('Status is:' . $e->getHttpStatus() . "\n");
                //print('Type is:' . $err['type'] . "\n");
                //print('Code is:' . $err['code'] . "\n");
                //print('Param is:' . $err['param'] . "\n");
                //print('Message is:' . $err['message'] . "\n");
                $response->setData([])->error($err['message'], 404)->flush();

            } catch (\Stripe\Error\RateLimit $e) {
                // Too many requests made to the API too quickly
                \Pimcore\Logger::critical($e);
                throw new \Exception('An application error occurred, please try again later...');
            } catch (\Stripe\Error\InvalidRequest $e) {
                // Invalid parameters were supplied to Stripe's API
                \Pimcore\Logger::critical($e);
                throw new \Exception('An application error occurred, please try again later...');
            } catch (\Stripe\Error\Authentication $e) {
                // Authentication with Stripe's API failed
                // (maybe you changed API keys recently)
                \Pimcore\Logger::critical($e);
                throw new \Exception('An application error occurred, please try again later...');
            } catch (\Stripe\Error\ApiConnection $e) {
                // Network communication with Stripe failed
                \Pimcore\Logger::critical($e);
                throw new \Exception('An application error occurred, please try again later...');
            } catch (\Stripe\Error\Base $e) {
                // Display a very generic error to the user, and maybe send
                // yourself an email
                \Pimcore\Logger::critical($e);
                throw new \Exception('An application error occurred, please try again later...');
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
                \Pimcore\Logger::critical($e);
                throw new \Exception('An application error occurred, please try again later...');
            }
        } catch (\Exception $e) {
            $response->error(Status::HTTP_INTERNAL_SERVER_ERROR)->debug($e);
        }
        $response->flush();
    }
}