<?php
use Vein\Http\Status;

/**
 * Class OaStripe_StripeController
 *
 * // reachable via /plugin/oa-stripe/stripe/index
 */
class OaStripe_StripeController extends \OaStripe\Controller\Action\Admin
{
    /**
     * Disable View Auto Renderer
     */
    public function init()
    {
        parent::init();
        $this->disableViewAutoRender();
    }

    /**
     * Update Configuration
     */
    public function updateConfigAction()
    {
        $response = $this->getResponder();

        try {
            $config = $this->getHttpParams();
            if (empty($config['credential'])) {
                $response->error('Credential settings not found')->flush();
            }

            \OaStripe\Stripe::updateConfig($config);

            $response->setData($config)->success();
        } catch (\Exception $e) {
            $response->error(Status::HTTP_INTERNAL_SERVER_ERROR)->debug($e);
        }
        $response->flush();
    }

    /**
     * Get Configuration
     */
    public function getConfigAction()
    {
        $response = $this->getResponder();

        try {
            $config = \OaStripe\Stripe::getConfig();
            if (!$config) {
                $config = [];
            }
            $response->setData($config)->success();
        } catch (\Exception $e) {
            $response->error(Status::HTTP_INTERNAL_SERVER_ERROR)->debug($e);
        }
        $response->flush();
    }

    /**
     * Plans Action
     */
    public function plansAction()
    {
        $response = $this->getResponder();

        try {
            $persister = new \OaStripe\Db\DataPersister();
            $plans = $persister->findAllPlans();

            if (empty($plans)) {
                $response->error('You don\'t have any plans yet')->flush();
            }


            $response->setData($plans)->success();
        } catch (\Exception $e) {
            $response->error(Status::HTTP_INTERNAL_SERVER_ERROR)->debug($e);
        }
        $response->flush();
    }

    /**
     * Update the plans to local database table
     */
    public function updatePlansAction()
    {
        $response = $this->getResponder();

        try {
            $plans = \Stripe\Plan::all(array("limit" => 20));
            if (empty($plans['data'])) {
                $response->error('You don\'t have any plans yet')->flush();
            }

            $data = $plans['data'];

            $persister = new \OaStripe\Db\DataPersister();
            /** @var \Stripe\Plan $planObject */
            foreach ($data as $planObject) {
                $persister->insertOrUpdatePlan($planObject);
            }

            $response->setData($plans)->success();
        } catch (\Exception $e) {
            $response->error(Status::HTTP_INTERNAL_SERVER_ERROR)->debug($e);
        }
        $response->flush();
    }
}
