<?php
namespace OaStripe;

use OaStripe\Controller\Plugin\AppFront;
use Pimcore\API\Plugin as PluginLib;

/**
 * Class Plugin
 *
 * @package OaStripe
 */
class Plugin extends PluginLib\AbstractPlugin implements PluginLib\PluginInterface
{
    /**
     * @return string
     */
    public static function install()
    {
        $path = self::getInstallPath();

        if (!is_dir($path . '/installed')) {
            if (!is_dir($path)) {
                mkdir($path);
            }
            mkdir($path . '/installed');

            if (!file_exists($path . '/version')) {
                self::executeSqlFromFile(self::pluginPath() . '/data/schema/install-mysql.sql');
                file_put_contents($path . '/version', '0.1');
            } else {
                //install database updates
            }
        }

        if (self::isInstalled()) {
            return "OaStripe Plugin successfully installed.";
        } else {
            return "OaStripe Plugin could not be installed";
        }
    }

    /**
     * @return string
     */
    public static function getInstallPath()
    {
        return PIMCORE_WEBSITE_VAR . '/plugins/oa-stripe';
    }

    /**
     * Execute SQL
     *
     * @param $filename
     * @throws \Exception
     */
    private static function executeSqlFromFile($filename)
    {
        if (!file_exists($filename)) {
            throw new \Exception('Unable to locate the sql file ' . $filename);
        }

        try {
            self::getDb()->exec(file_get_contents($filename));
        } catch (\Zend_Db_Statement_Exception $e) {
            throw new \Exception("Unable to execute the sql file \n" . $filename . "\n" . $e->getMessage());
        }
    }

    /**
     * Get Plugin Path
     *
     * @return string
     */
    public static function pluginPath()
    {
        return PIMCORE_PLUGINS_PATH . '/OaStripe';
    }

    /**
     * @return bool
     */
    public static function isInstalled()
    {
        return is_dir(static::getInstallPath() . '/installed');
    }

    /**
     * @return string
     */
    public static function uninstall()
    {
        rmdir(self::getInstallPath() . '/installed');
        self::executeSqlFromFile(self::pluginPath() . '/data/schema/uninstall-mysql.sql');

        if (!self::isInstalled()) {
            return "OaStripe Plugin successfully uninstalled.";
        } else {
            return "OaStripe Plugin could not be uninstalled";
        }
    }

    /**
     * @throws \Zend_EventManager_Exception_InvalidArgumentException
     */
    public function init()
    {
        parent::init();
        \Pimcore::getEventManager()->attach("system.startup", [$this, "systemStartup"], 9999);
    }

    /**
     * @param $e
     */
    public function systemStartup($e)
    {
        if (Stripe::getConfig()) {
            Stripe::initConfig();

            $front = \Zend_Controller_Front::getInstance();
            $frontend = \Pimcore\Tool::isFrontend();

            $front->registerPlugin(new AppFront());
        }

        $router = \Zend_Controller_Front::getInstance()->getRouter();
        $route = new \Zend_Controller_Router_Route(
            'oa-stripe/:action/*',
            [
                'module' => 'OaStripe',
                'controller' => 'front',
                'action' => 'default',
            ]
        );
        $router->addRoute('StripeRoute', $route);
    }
}
