<?php

namespace OaStripe\Controller\Action;

use Vein\Http\Json;
use Vein\Http\Status;
use Vein\Http\Utility;

/**
 * Class Admin
 *
 * @package OaStripe\Controller\Action
 */
class Admin extends \Pimcore\Controller\Action\Admin
{
    protected $httpJson;
    protected $httpUtility;

    /**
     * @return mixed
     */
    public function getHttpParams($key = null)
    {
        $data = $this->getHttpUtility()->getJsonData();
        if (null === $key) {
            return $data;
        }

        return $data[$key];
    }

    /**
     * @return Utility
     */
    public function getHttpUtility()
    {
        if (null === $this->httpUtility) {
            $this->httpUtility = new Utility($this->getRequest());
        }

        return $this->httpUtility;
    }

    /**
     * @param \Zend_Form $form
     * @return mixed
     */
    public function getValidValues(\Zend_Form $form, $params = null)
    {
        if (!$this->getRequest()->isPost()) {
            $this->getResponder()->error(Status::HTTP_BAD_REQUEST)->flush();
        }

        //Get the post values
        if (null === $params) {
            $params = $this->getHttpUtility()->getJsonData();
        }

        if (!$form->isValid($params)) {
            $messages = $form->getMessages();
            $this->getResponder()->error(Status::HTTP_UNPROCESSABLE_ENTITY)->setData($messages)->debug($messages)->flush();
        }

        return $form->getValues();
    }

    /**
     * @return Json
     */
    public function getResponder()
    {
        if (null === $this->httpJson) {
            $this->httpJson = new Json();
        }

        return $this->httpJson;
    }
}