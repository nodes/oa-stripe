<?php
namespace OaStripe\Controller\Plugin;

use OaStripe\Stripe;
use Pimcore\Tool;
use Thunder\Shortcode\HandlerContainer\HandlerContainer;
use Thunder\Shortcode\Parser\RegularParser;
use Thunder\Shortcode\Processor\Processor;
use Thunder\Shortcode\Shortcode\ShortcodeInterface;

/**
 * Class AppFront
 *
 * @package OaStripe\Controller\Plugin
 */
class AppFront extends \Zend_Controller_Plugin_Abstract
{
    /**
     * @var bool
     */
    protected $enabled = true;

    /**
     * @return bool
     */
    public function disable()
    {
        $this->enabled = false;
        return true;
    }

    /**
     * @param \Zend_Controller_Request_Abstract $request
     */
    public function preDispatch(\Zend_Controller_Request_Abstract $request)
    {
        if ($request->getModuleName() == 'OaStripe') {
            if ($request->getControllerName() == 'front' && $request->getActionName() == 'confirm') {
                $rt = Stripe::getCheckoutScript();
                if ($rt['module']) {
                    $request->setModuleName($rt['module']);
                }

                if ($rt['controller']) {
                    $request->setControllerName($rt['controller']);
                }

                if ($rt['action']) {
                    $request->setActionName($rt['action']);
                }
            }
        }
    }

    /**
     * Replace the contents
     */
    public function dispatchLoopShutdown()
    {
        if (!Tool::isFrontend()) {
            return;
        }

        if (!Tool::isHtmlResponse($this->getResponse())) {
            return;
        }

        $editmode = \Zend_Registry::get("pimcore_editmode");
        if ($editmode) {
            return;
        }

        $body = $this->getResponse()->getBody();

        if (Stripe::getConfig()) {
            $modified = false;

            if (strpos($body, 'oa-stripe-form') || strpos($body, 'oa-stripe-qc') || strpos($body, 'oa-stripe-ng-check')) {
                $code = '';

                if (strpos($body, 'oa-stripe-form')) {
                    $code = "\n" . '<script type="text/javascript" src="https://js.stripe.com/v2/"></script>' . "\n";
                    $code .= '<script type="text/javascript">' . "\n";
                    $code .= "  Stripe.setPublishableKey('" . Stripe::getPublishableKey() . "');" . "\n";
                    $code .= '</script>' . "\n";
                }

                if (strpos($body, 'oa-stripe-qc') || strpos($body, 'oa-stripe-ng-check')) {
                    $code .= "\n" . '<script type="text/javascript" src="https://checkout.stripe.com/checkout.js"></script>' . "\n";
                }

                $code .= "\n" . '<script type="text/javascript" src="/plugins/OaStripe/static/js/stripe.js"></script>' . "\n";

                $headEndPosition = stripos($body, "</body>");
                if ($headEndPosition !== false) {
                    $body = substr_replace($body, $code . "</body>", $headEndPosition, 7);
                }

                $modified = true;
            }

            if ($modified) {
                $this->getResponse()->setBody($body);
            }
        }
        return;
    }

}
