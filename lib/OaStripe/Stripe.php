<?php

namespace OaStripe;

use OaStripe\Db\DataPersister;

/**
 * Class Stripe
 *
 * @package OaStripe
 */
class Stripe extends \Stripe\Stripe
{
    /**
     * @var
     */
    protected static $instance;

    /**
     * @var
     */
    protected static $config;

    /**
     * @var
     */
    protected static $persister;

    /**
     * Update Config
     *
     * @param array $config
     * @return array
     */
    public static function updateConfig(array $config)
    {
        $filename = Plugin::getInstallPath() . '/configuration.php';
        file_put_contents($filename, '<?php return ' . var_export($config, true) . ';');
        return $config;
    }

    /**
     * Initiate configuration
     */
    public static function initConfig()
    {
        if (null === self::$instance) {
            $config = self::getConfig();
            if (!$config) {
                throw new \Exception('Stripe configuration file not found');
            }

            $credential = $config['credential'];
            if ($credential['mode'] == 'live') {
                $key = $credential['liveSecretKey'];
            } else {
                $key = $credential['testSecretKey'];
            }
            self::setApiKey($key);
        }
        self::$instance = true;
    }

    /**
     * Get Configuration
     *
     * @return mixed
     * @throws \Exception
     */
    public static function getConfig($name = null)
    {
        if (null === self::$config) {
            $filename = Plugin::getInstallPath() . '/configuration.php';
            if (!file_exists($filename)) {
                return false;
            }
            self::$config = include $filename;
        }
        return (null === $name) ? self::$config : self::$config[$name];
    }

    /**
     * Get Secret Key
     *
     * @return mixed
     */
    public static function getSecretKey()
    {
        $credential = self::getConfig('credential');
        if (self::isLiveMode()) {
            return $credential['liveSecretKey'];
        } else {
            return $credential['testSecretKey'];
        }
    }

    /**
     * Get Configuration
     */
    public static function isLiveMode()
    {
        $credential = self::getConfig('credential');
        return ($credential['mode'] == 'live');
    }

    /**
     * Get Configuration
     */
    public static function getRedirectUrl()
    {
        $ui = self::getConfig('ui');
        return empty($ui['paymentSuccess']) ? false : $ui['paymentSuccess'];
    }

    /**
     * Checkout script
     */
    public static function getCheckoutScript()
    {
        $ui = self::getConfig('ui');
        return [
            'module' => ($ui['checkoutModule']) ? $ui['checkoutModule'] : false,
            'controller' => ($ui['checkoutController']) ? $ui['checkoutController'] : false,
            'action' => ($ui['checkoutAction']) ? $ui['checkoutAction'] : false,
        ];
    }

    /**
     * @param $email
     * @param $plan
     * @param $quantity
     * @param array $metadata
     * @return string
     * @throws \Exception
     */
    public static function newTransaction($email, $plan, $quantity, $metadata = [])
    {
        if (empty($plan)) {
            throw new \Exception('Plan cannot be empty');
        }

        if (empty($quantity) || !is_numeric($quantity)) {
            throw new \Exception('Invalid quantity!');
        }

        if (!\Zend_Validate::is($email, 'EmailAddress')) {
            throw new \Exception('Invalid email address!');
        }

        //verify plan
        $dataPersister = new \OaStripe\Db\DataPersister();
        $planId = $plan;
        $plan = $dataPersister->findPlanById($plan);

        if (!$plan) {
            throw new \Exception('Invalid plan');
        }

        $key = 'tx_' . uniqid();

        $oaStripeTx = new \Zend_Session_Namespace('oaStripeTx');
        //$oaStripeTx->setExpirationSeconds(1800, $key);
        $oaStripeTx->{$key} = [
            'email' => $email,
            'plan' => $planId,
            'quantity' => $quantity,
            'metadata' => $metadata
        ];

        return $key;
    }

    /**
     * Get Transaction
     *
     * @param $txId
     * @return bool
     */
    public static function getTransaction($txId)
    {
        $oaStripeTx = new \Zend_Session_Namespace('oaStripeTx');
        return (isset($oaStripeTx->{$txId})) ? $oaStripeTx->{$txId} : false;
    }

    /**
     * Update Transaction
     */
    public static function updateTransaction($txId, $data)
    {
        $oaStripeTx = new \Zend_Session_Namespace('oaStripeTx');
        if (!isset($oaStripeTx->{$txId})) {
            throw new \Exception('Update of transaction failed');
        }
        $oaStripeTx->{$txId} = $data;
        return $data;
    }

    /**
     * Get Transaction
     *
     * @param $txId
     * @return bool
     */
    public static function deleteTransaction($txId)
    {
        $oaStripeTx = new \Zend_Session_Namespace('oaStripeTx');
        unset($oaStripeTx->{$txId});
        return true;
    }

    /**
     * Subscription checkout
     */
    public static function subscriptionCheckout()
    {

    }

    /**
     * Amount to be charged
     */
    public static function quickCheckoutButton($label, $description, $amount, array $options = array())
    {
        $qcConfig = self::getConfig('quickCheckout');

        $options = array_merge([
            'name' => $qcConfig['name'],
            'description' => $description,
            'amount' => $amount
        ], $options);

        $uniqueId = 'oaStripeQC_' . uniqid();

        $c = json_encode([
            'image' => $qcConfig['logo'],
            'key' => self::getPublishableKey()
        ]);

        $optionsEncode = json_encode($options);
        return "<button id=\"{$uniqueId}\" class='oa-stripe-qc' data-amount='{$amount}' data-config='{$c}' data-options='{$optionsEncode}'>{$label}</button>";
    }

    //--------------------------------------------------------------------------------- SINGLE PAYMENT CHECKOUT --------

    /**
     * Get Configuration
     */
    public static function getPublishableKey()
    {
        $credential = self::getConfig('credential');
        if (self::isLiveMode()) {
            return $credential['livePublishableKey'];
        } else {
            return $credential['testPublishableKey'];
        }
    }

    /**
     * Quick check out chrge customer
     *
     * @param $token
     * @param $email
     * @param $amount
     * @return \Stripe\Charge
     */
    public static function quickCheckoutCharge($token, $email, $amount)
    {
        $customer = self::getDataManager()->findCustomerByEmail($email);
        if (!$customer) {
            $customer = \Stripe\Customer::create([
                "source" => $token,
                "email" => $email
            ]);

            self::getDataManager()->createCustomer($customer);
        } else {
            $customer = \Stripe\Customer::retrieve($customer->id);
        }

        $customerArray = $customer->jsonSerialize();

        $charge = \Stripe\Charge::create(array(
            "amount" => $amount,
            "currency" => "usd",
            "source" => $token,
            "description" => "Charge for {$email}",
            "customer" => $customer->id
        ));

        $chargeArray = self::getDataManager()->createCharge($charge);
        return $charge;
    }

    /**
     * @return DataPersister
     */
    public static function getDataManager()
    {
        if (null === self::$persister) {
            self::$persister = new DataPersister();
        }
        return self::$persister;
    }
}