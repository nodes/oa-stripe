<?php

namespace OaStripe\Db;

use OaStripe\Db\Table\StripeCharge;
use OaStripe\Db\Table\StripeCustomer;
use OaStripe\Db\Table\StripePlan;
use OaStripe\Db\Table\StripeSubscription;
use OaStripe\Stripe;
use Pimcore\Resource\Mysql;

/**
 * Class DataPersister
 * @package OaStripe\Legacy
 */
class DataPersister
{
    /**
     * @var
     */
    protected static $adapter;

    /**
     * DataPersister constructor.
     */
    public function __construct()
    {
        if (null === self::$adapter) {
            \Zend_Db_Table::setDefaultAdapter(Mysql::get()->getResource());
            self::$adapter = true;
        }
    }

    /**
     * @param \Stripe\Plan $plan
     * @return array
     */
    public function insertOrUpdatePlan(\Stripe\Plan $plan)
    {
        $table = new StripePlan();
        $plan = $plan->jsonSerialize();
        $plan['metadata'] = json_encode($plan['metadata']);

        if (!$this->findPlanById($plan['id'], $plan['livemode'])) {
            $table->insert($plan);
        } else {
            $where['livemode = ?'] = (bool)$plan['livemode'];
            $where['id = ?'] = (string)$plan['id'];
            $table->update($plan, $where);
        }
        return $plan;
    }

    /**
     * Find plan by id
     *
     * @param $id
     * @param $liveMode
     * @return null|\Zend_Db_Table_Row_Abstract
     */
    public function findPlanById($id, $liveMode = null)
    {
        if (null === $liveMode) {
            $liveMode = (Stripe::isLiveMode()) ? 1 : 0;
        }

        $table = new StripePlan();
        return $table->fetchRow(
            $table->select()->where('livemode = ?', $liveMode)
                ->where('id = ?', (string)$id)
        );
    }

    /**
     * Create Customer
     *
     * @param \Stripe\Customer $customer
     * @return \Stripe\Customer
     */
    public function createCustomer(\Stripe\Customer $customer)
    {
        $valid = [
            'id', 'account_balance', 'created', 'currency', 'default_source', 'email',
            'livemode', 'metadata', 'sources', 'subscriptions'
        ];

        $customer = $customer->jsonSerialize();

        $data = [];
        foreach ($customer as $key => $value) {
            if (in_array($key, $valid)) {
                if (in_array($key, ['metadata', 'sources', 'subscriptions']) && !empty($value)) {
                    $data[$key] = json_encode($value);
                } else {
                    $data[$key] = $value;
                }
            }
        }

        $table = new StripeCustomer();
        $table->insert($data);
        return $customer;
    }

    /**
     * @param \Stripe\Charge $charge
     * @return mixed
     */
    public function createCharge(\Stripe\Charge $charge)
    {
        $valid = [
            'charge_id', 'id', 'amount', 'amount_refunded', 'application_fee', 'balance_transaction', 'captured',
            'created', 'currency', 'customer', 'description', 'destination', 'dispute', 'failure_code',
            'failure_message', 'fraud_details', 'order', 'livemode', 'paid', 'receipt_number', 'invoice',
            'receipt_email', 'metadata', 'refunded', 'source_transfer', 'shipping', 'refunds', 'source',
            'statement_descriptor', 'status'
        ];

        $chargeArray = $charge->jsonSerialize();

        $data = [];
        foreach ($chargeArray as $key => $value) {
            if (in_array($key, $valid)) {
                if (in_array($key, ['fraud_details', 'metadata', 'refunds', 'source']) && !empty($value)) {
                    $data[$key] = json_encode($value);
                } else {
                    $data[$key] = $value;
                }
            }
        }

        $table = new StripeCharge();
        $table->insert($data);
        return $chargeArray;
    }

    /**
     * Create or update subscription
     */
    public function createSubscription(\Stripe\Subscription $subscription)
    {
        $valid = [
            'id', 'application_fee_percent', 'cancel_at_period_end', 'canceled_at',
            'created', 'current_period_end', 'current_period_start', 'customer', 'discount',
            'ended_at', 'livemode', 'metadata', 'plan', 'quantity', 'start', 'status',
            'tax_percent', 'trial_start', 'trial_end'
        ];

        $subscription = $subscription->jsonSerialize();

        //find the customer
        $customer = $this->findCustomerById($subscription['customer']);
        if (!$customer) {
            throw new \Exception('Customer data not found');
        }

        $data = [
            'email' => $customer['email']
        ];

        if (is_array($subscription['plan'])) {
            $subscription['plan'] = $subscription['plan']['id'];
        }

        foreach ($subscription as $key => $value) {
            if (in_array($key, $valid)) {
                if (in_array($key, ['metadata']) && !empty($value)) {
                    $data[$key] = json_encode($value);
                } else {
                    $data[$key] = $value;
                }
            }
        }

        $table = new StripeSubscription();
        $table->insert($data);
        return $subscription;
    }

    /**
     * @param $id
     * @param null $liveMode
     * @return null|\Zend_Db_Table_Row_Abstract
     */
    public function findCustomerById($id, $liveMode = null)
    {
        if (null === $liveMode) {
            $liveMode = (Stripe::isLiveMode()) ? 1 : 0;
        }

        $table = new StripeCustomer();
        return $table->fetchRow(
            $table->select()->where('livemode = ?', $liveMode)
                ->where('id = ?', (string)$id)
        );
    }

    /**
     * @param $id
     * @param null $liveMode
     * @return null|\Zend_Db_Table_Row_Abstract
     */
    public function findSubscriptionById($id, $liveMode = null)
    {
        if (null === $liveMode) {
            $liveMode = (Stripe::isLiveMode()) ? 1 : 0;
        }

        $table = new StripeSubscription();
        return $table->fetchRow(
            $table->select()->where('livemode = ?', $liveMode)
                ->where('id = ?', (string)$id)
        );
    }

    /**
     * @param $email
     * @param null $liveMode
     * @return null|\Zend_Db_Table_Row_Abstract
     */
    public function findCustomerByEmail($email, $liveMode = null)
    {
        if (null === $liveMode) {
            $liveMode = (Stripe::isLiveMode()) ? 1 : 0;
        }

        $table = new StripeCustomer();
        return $table->fetchRow(
            $table->select()->where('livemode = ?', $liveMode)
                ->where('email = ?', (string)$email)
        );
    }

    /**
     * Find Subscription by email
     *
     * @param $email
     * @param null $liveMode
     * @return mixed
     */
    public function findSubscriptionByEmail($email, $liveMode = null)
    {
        if (null === $liveMode) {
            $liveMode = (Stripe::isLiveMode()) ? 1 : 0;
        }

        $table = new StripeSubscription();
        return $table->fetchRow(
            $table->select()->where('livemode = ?', $liveMode)
                ->where('email = ?', (string)$email)
        );
    }

    /**
     * @return array|bool
     */
    public function findAllPlans()
    {
        $table = new StripePlan();
        $result = $table->fetchAll();
        if (!$result) {
            return false;
        }
        return $result->toArray();
    }
}