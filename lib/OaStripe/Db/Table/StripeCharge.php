<?php

namespace OaStripe\Db\Table;

/**
 * Class StripeCharge
 * @package OaStripe\Db\Table
 */
class StripeCharge extends \Zend_Db_Table_Abstract
{
    protected $_name = 'oastripe_charges';
}