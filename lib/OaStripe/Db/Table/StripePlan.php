<?php

namespace OaStripe\Db\Table;

/**
 * Class StripePlan
 * @package OaStripe\Db\Table
 */
class StripePlan extends \Zend_Db_Table_Abstract
{
    protected $_name = 'oastripe_plans';
}