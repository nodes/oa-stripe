<?php

namespace OaStripe\Db\Table;

/**
 * Class StripeCustomer
 * @package OaStripe\Db\Table
 */
class StripeCustomer extends \Zend_Db_Table_Abstract
{
    protected $_name = 'oastripe_customers';
}