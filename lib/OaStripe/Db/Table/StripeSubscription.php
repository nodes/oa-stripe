<?php

namespace OaStripe\Db\Table;

/**
 * Class StripeSubscription
 * @package OaStripe\Db\Table
 */
class StripeSubscription extends \Zend_Db_Table_Abstract
{
    protected $_name = 'oastripe_subscriptions';
}