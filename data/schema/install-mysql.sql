CREATE TABLE IF NOT EXISTS `oastripe_plans` (
  `pid`                  INT(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id`                   VARCHAR(32)     NOT NULL,
  `object`               VARCHAR(16)              DEFAULT NULL,
  `amount`               FLOAT(6, 2)              DEFAULT NULL,
  `created`              BIGINT(20)               DEFAULT NULL,
  `currency`             VARCHAR(5)               DEFAULT NULL,
  `interval`             VARCHAR(16)              DEFAULT NULL,
  `interval_count`       INT(5) UNSIGNED          DEFAULT NULL,
  `livemode`             SMALLINT(1)     NOT NULL,
  `metadata`             TEXT,
  `name`                 VARCHAR(64)              DEFAULT NULL,
  `statement_descriptor` VARCHAR(64)              DEFAULT NULL,
  `trial_period_days`    INT(3) UNSIGNED          DEFAULT NULL,
  PRIMARY KEY (`pid`),
  UNIQUE KEY `unique_index` (`id`, `livemode`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `oastripe_customers` (
  `customer_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id` varchar(32) NOT NULL,
  `account_balance` float(8,2) DEFAULT NULL,
  `created` bigint(20) unsigned DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `default_source` varchar(64) DEFAULT NULL,
  `email` varchar(120) NOT NULL,
  `livemode` tinyint(1) DEFAULT NULL,
  `metadata` text,
  `sources` text,
  `subscriptions` text,
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `oastripe_subscriptions` (
  `subscription_id` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(32) NOT NULL,
  `application_fee_percent` int(4) unsigned DEFAULT NULL,
  `cancel_at_period_end` tinyint(1) DEFAULT NULL,
  `canceled_at` bigint(20) unsigned DEFAULT NULL,
  `created` bigint(20) unsigned DEFAULT NULL,
  `current_period_end` bigint(20) unsigned DEFAULT NULL,
  `current_period_start` bigint(20) unsigned DEFAULT NULL,
  `customer` varchar(32) NOT NULL,
  `discount` varchar(32) DEFAULT NULL,
  `ended_at` bigint(20) unsigned DEFAULT NULL,
  `livemode` tinyint(4) NOT NULL,
  `metadata` text,
  `plan` varchar(32) NOT NULL,
  `quantity` int(11) NOT NULL,
  `start` bigint(20) unsigned DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  `tax_percent` varchar(10) DEFAULT NULL,
  `trial_start` bigint(20) unsigned DEFAULT NULL,
  `trial_end` bigint(20) unsigned DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  PRIMARY KEY (`subscription_id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- < ver 0.2

CREATE TABLE IF NOT EXISTS `oastripe_charges` (
  `charge_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id` varchar(32) NOT NULL,
  `amount` float(7,2) NOT NULL,
  `amount_refunded` float(7,2) DEFAULT NULL,
  `application_fee` float(7,2) DEFAULT NULL,
  `balance_transaction` varchar(64) DEFAULT NULL,
  `captured` tinyint(1) DEFAULT NULL,
  `created` bigint(20) unsigned DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `customer` varchar(64) DEFAULT NULL,
  `description` varchar(128) DEFAULT NULL,
  `destination` varchar(128) DEFAULT NULL,
  `dispute` varchar(32) DEFAULT NULL,
  `failure_code` varchar(10) DEFAULT NULL,
  `failure_message` varchar(128) DEFAULT NULL,
  `fraud_details` varchar(250) DEFAULT NULL,
  `order` varchar(32) DEFAULT NULL,
  `livemode` tinyint(1) DEFAULT NULL,
  `paid` tinyint(1) DEFAULT NULL,
  `receipt_number` varchar(32) DEFAULT NULL,
  `invoice` varchar(64) DEFAULT NULL,
  `receipt_email` varchar(128) DEFAULT NULL,
  `metadata` varchar(250) DEFAULT NULL,
  `refunded` tinyint(1) DEFAULT NULL,
  `source_transfer` varchar(32) DEFAULT NULL,
  `shipping` text,
  `refunds` varchar(250) DEFAULT NULL,
  `source` text,
  `statement_descriptor` varchar(200) DEFAULT NULL,
  `status` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`charge_id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- > ver 0.2