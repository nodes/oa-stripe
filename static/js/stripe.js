$(function () {
    var $form = $('.oa-stripe-form');

    $('#oaStripeErrorMsg').hide();

    $form.submit(function (event) {
        $('#oaStripeErrorMsg').hide();
        // Disable the submit button to prevent repeated clicks:
        $form.find('.submit').prop('disabled', true);

        // Request a token from Stripe:
        Stripe.card.createToken($form, stripeResponseHandler);

        // Prevent the form from being submitted:
        return false;
    });

    function stripeResponseHandler(status, response) {
        var $form = $('.oa-stripe-form');

        if (response.error) {
            $form.find('#oaStripeErrorMsg').text(response.error.message).show();
            $form.find('.submit').prop('disabled', false);
        } else {
            var token = response.id;
            //$form.append($('<input type="hidden" name="stripeToken">').val(token));
            //$form.get(0).submit();

            var paramObj = {};
            $.each($form.serializeArray(), function (_, kv) {
                paramObj[kv.name] = kv.value;
            });
            response['form'] = paramObj;

            $.post("/oa-stripe/token", response, function (r) {
                var data = r.data;
                if (data.redirect) {
                    window.location.href = data.redirect;
                }
                $form.find('.submit').prop('disabled', false);

                $('body').trigger("oaStripeTxSuccess", r);
            }).fail(function (r) {
                if (r.responseJSON && r.status == 404) {
                    $('#oaStripeErrorMsg').text(r.responseJSON.message).show();
                }
                $form.find('.submit').prop('disabled', false);
            });
        }
    }

    //Quick checkout
    var qcHandler;

    $('.oa-stripe-qc').on('click', function ($e) {

        var o = $(this).data('options');
        var amount = $(this).data('amount');

        qcHandler.open({
            name: o.name,
            description: o.description,
            amount: amount
        });
        $e.preventDefault();
    });

    if ($('.oa-stripe-qc').length > 0 || $('.oa-stripe-qc-input').length > 0) {
        var config = $('.oa-stripe-qc').first().data('config');
        insertCheckoutScript(config);
    }

    /**
     * Insert checkout script
     */
    function insertCheckoutScript(config) {
        if (!$('body').data('oaStripeQc')) {
            qcHandler = StripeCheckout.configure({
                key: config.key,
                image: config.image,
                locale: 'auto',
                token: function (token) {
                    $('body').trigger("oaStripeQcSuccess", token);
                }
            });

            window.addEventListener('popstate', function () {
                qcHandler.close();
            });

            $('body').data('oaStripeQc', true);
        }
    }
});