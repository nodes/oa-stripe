pimcore.registerNS("pimcore.plugin.OaStripe");

pimcore.plugin.OaStripe = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.OaStripe";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("OaStripe Plugin Ready!");
    }
});

var OaStripePlugin = new pimcore.plugin.OaStripe();

