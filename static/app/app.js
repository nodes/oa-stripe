(function () {
    'use strict';

    /**
     * Angular App
     */
    angular.module('app', [
        'ui.bootstrap'
    ]);

    /**
     * ConfigurationController
     * -----------------------------------------------------------------------------------------------------------------
     */
    angular.module('app').controller('ConfigurationController', [
        '$scope', '$rootScope', 'StripeService',

        function ($scope, $rootScope, StripeService) {

            $scope.progress = false;

            $scope.config = {
                credential: {}
            };
            StripeService.getConfig({}).then(function (r) {
                $scope.config = r.data;
            });

            $scope.saveConfig = function (config, form) {
                form.$setSubmitted();
                if (!form.$valid) {
                    return false;
                }

                console.log(config);

                $scope.progress = true;
                StripeService.updateConfig(config).then(function (r) {
                    $scope.progress = false;
                });
            };

        }
    ]);

    /**
     * Plan Controller
     * -----------------------------------------------------------------------------------------------------------------
     */
    angular.module('app').controller('PlanController', [
        '$scope', '$rootScope', 'StripeService',

        function ($scope, $rootScope, StripeService) {

            $scope.progress = false;
            $scope.plans = false;

            StripeService.findPlans({}).then(function (r) {
                $scope.plans = r.data;
            }, function (r) {

            });

            $scope.updatePlans = function () {
                StripeService.updatePlans({}).then(function (r) {

                });
            };

        }
    ]);

    /**
     * StripeService Service
     * -----------------------------------------------------------------------------------------------------------------
     */
    angular.module('app').factory('StripeService', [
        '$http',

        function ($http) {
            var s = {
                url: '/plugin/OaStripe'
            };

            s.getConfig = function (params) {
                return $http.post(s.url + '/stripe/get-config', params).then(function (r) {
                    return r.data;
                });
            };

            s.updateConfig = function (params) {
                return $http.post(s.url + '/stripe/update-config', params).then(function (r) {
                    return r.data;
                });
            };

            s.updatePlans = function (params) {
                return $http.post(s.url + '/stripe/update-plans', params).then(function (r) {
                    return r.data;
                });
            };

            s.findPlans = function (params) {
                return $http.post(s.url + '/stripe/plans', params).then(function (r) {
                    return r.data;
                });
            };

            return s;
        }
    ]);

})();