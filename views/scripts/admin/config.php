<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OA Stripe</title>

    <link href="/plugins/OaStripe/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/plugins/OaStripe/static/css/stripe.css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top hide">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Dashboard</a></li>
                <li><a href="#">Settings</a></li>
                <li><a href="#">Profile</a></li>
                <li><a href="#">Help</a></li>
            </ul>
        </div>
    </div>
</nav>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12 col-md-12 main">

            <? include 'includes/credential.php'; ?>

            <? include 'includes/plan.php'; ?>

            <? include 'includes/ui.php'; ?>

            <? include 'includes/quick-checkout.php'; ?>

        </div>
    </div>
</div>


<script src="/plugins/OaStripe/static/bootstrap/js/jquery-3.1.1.min.js"></script>
<script src="/plugins/OaStripe/static/js/angular.min.js"></script>
<script src="/plugins/OaStripe/static/js/ui-bootstrap-tpls-2.1.4.min.js"></script>
<script src="/plugins/OaStripe/static/app/app.js"></script>
</body>
</html>