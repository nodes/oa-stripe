<div ng-controller="PlanController">
    <uib-accordion close-others="oneAtATime">
        <div uib-accordion-group class="panel-default">

            <uib-accordion-heading>
                Plans
                <button ng-click="updatePlans()" class="pull-right btn btn-default btn-xs">Pull Updates</button>
            </uib-accordion-heading>

            <div ng-if="!plans">
                <p>You don't have any plans yet</p>
            </div>

            <div ng-if="plans">
                <table class="table">
                    <thead>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Currency</th>
                    <th>Interval</th>
                    <th>Live Mode</th>
                    </thead>

                    <tbody>
                    <tr ng-repeat="plan in plans">
                        <td>{{ plan.id }}</td>
                        <td>{{ plan.name }}</td>
                        <td>{{ plan.amount / 100 }}</td>
                        <td>{{ plan.currency }}</td>
                        <td>{{ plan.interval }}</td>
                        <td>{{ plan.livemode }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </uib-accordion>
</div>