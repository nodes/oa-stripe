<div ng-controller="ConfigurationController">
    <uib-accordion close-others="oneAtATime">
        <div uib-accordion-group class="panel-default">

            <uib-accordion-heading>
                Checkout
            </uib-accordion-heading>

            <div>
                <form name="configUiForm" class="form-horizontal" ng-submit="saveConfig(config, configUiForm)">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Checkout Action</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" ng-model="config.ui.checkoutModule"
                                   placeholder="Module">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" ng-model="config.ui.checkoutController"
                                   placeholder="Controller">
                        </div>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" ng-model="config.ui.checkoutAction"
                                   placeholder="Action">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Success redirect</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" ng-model="config.ui.paymentSuccess"
                                   placeholder="/payment/success">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10 text-right">
                            <button type="submit" class="btn btn-default" ng-disabled="progress">Save</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </uib-accordion>
</div>