<div ng-controller="ConfigurationController">
    <uib-accordion close-others="oneAtATime">
        <div uib-accordion-group class="panel-default" heading="Credentials">
            <form name="configForm" class="form-horizontal" ng-submit="saveConfig(config, configForm)">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Test or Live Mode</label>
                    <div class="col-sm-7">
                        <div class="btn-group">
                            <label class="btn btn-default" ng-model="config.credential.mode"
                                   uib-btn-radio="'test'" required>Test</label>
                            <label class="btn btn-default" ng-model="config.credential.mode"
                                   uib-btn-radio="'live'" required>Live</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Test Secret Key</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" ng-model="config.credential.testSecretKey"
                               placeholder="sk_test_xxxx" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Test Publishable Key</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               ng-model="config.credential.testPublishableKey" placeholder="pk_test_xxxx"
                               required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Live Secret Key</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" ng-model="config.credential.liveSecretKey"
                               placeholder="sk_live_xxxx" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Live Publishable Key</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               ng-model="config.credential.livePublishableKey" placeholder="pk_live_xxxx"
                               required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Transaction Secret</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               ng-model="config.credential.transactionSecret" placeholder="34r78hunjcdc34ef"
                               required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10 text-right">
                        <button type="submit" class="btn btn-default" ng-disabled="progress">Save</button>
                    </div>
                </div>
            </form>


        </div>
    </uib-accordion>
</div>