<div ng-controller="ConfigurationController">
    <uib-accordion close-others="oneAtATime">
        <div uib-accordion-group class="panel-default" heading="Quick Checkout">


            <form name="quickCheckoutForm" class="form-horizontal" ng-submit="saveConfig(config, quickCheckoutForm)">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Store Name</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               ng-model="config.quickCheckout.name" placeholder="My Store" required>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Logo</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control"
                               ng-model="config.quickCheckout.logo" placeholder="//assets/img/location/logo.jpg"
                               required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10 text-right">
                        <button type="submit" class="btn btn-default" ng-disabled="progress">Save</button>
                    </div>
                </div>
            </form>


        </div>
    </uib-accordion>
</div>