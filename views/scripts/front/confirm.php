<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>OA Stripe</title>

    <link href="/plugins/OaStripe/static/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/plugins/OaStripe/static/css/stripe.css" rel="stylesheet">
</head>
<body>

<form action="#" method="POST" id="payment-form" class="form-horizontal oa-stripe-form" role="form">

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 main">
                <div class="container" style="max-width: 700px;margin-top: 50px;">

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <p class="lead"><?= $this->plan['name'] ?></p>
                                </div>
                                <div class="col-sm-6">
                                    <p class="lead text-right">
                                        <small><?= strtoupper($this->plan['currency']) ?></small> <?= $this->total ?><?= (($this->plan['interval']) ? '/' . $this->plan['interval'] : null) ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p id="oaStripeErrorMsg" class="alert alert-warning">

                    </p>

                    <div class="panel panel-default">
                        <div class="panel-body">

                            <input type="hidden" class="form-control" name="do" value="<?= $this->do ?>">
                            <input type="hidden" class="form-control" name="tx" value="<?= $this->tx ?>">

                            <fieldset>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="card-holder-name">CARDHOLDER'S
                                        NAME</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" data-stripe="name" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="card-number">CARD NUMBER</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" size="20" data-stripe="number" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="expiry-month">EXPIRY DATE</label>
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <div class="col-xs-3">
                                                <input type="text" class="form-control" size="2" data-stripe="exp_month"
                                                       placeholder="MM" required>
                                            </div>
                                            <div class="col-xs-3">
                                                <input type="text" class="form-control" size="2" data-stripe="exp_year"
                                                       placeholder="YY" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" for="cvv">CVV/CVC</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" size="4" data-stripe="cvc" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9 text-right">
                                        <button type="submit" class="btn btn-success submit">Pay Now</button>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</form>

<script src="/plugins/OaStripe/static/bootstrap/js/jquery-3.1.1.min.js"></script>

</body>
</html>